from django.db import models

# Create your models here.

class Data(models.Model):
    title = models.CharField(max_length = 100)
    author = models.CharField(max_length = 100)
    likes = models.IntegerField()

    class Meta:
        ordering = ['-likes']
        
    def __str__(self):
        return self.title
