from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Data
from django.core import serializers
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def index(request):

    top5 = {'context' : Data.objects.all()[:5]}
    if request.method == 'POST':
        title = request.POST["title"]
        author = request.POST["author[]"]
    else:
        Data.objects.all().delete()

    return render(request, 'main/index.html', top5)
