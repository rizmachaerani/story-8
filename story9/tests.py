from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.

class Story9UnitTest(TestCase):
    def test_story9_url_exists(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_template_used(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'home.html')

    def test_story9_login_url(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_login_template_used(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_story9_login_success_url(self):
        response = Client().get('/story9/loginFinish/')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_login_success_template_used(self):
        response = Client().get('/story9/loginFinish/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_story9_signup_url(self):
        response = Client().get('/story9/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_signup_template_used(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    # def test_story9_can_login(self):
    #     user = User.objects.create_user('boba','qwerty123')
    #     response = Client().post('/login/', {'user_id':'boba', 'password':'qwerty123'})
    #     self.assertEqual(response.status_code,302)

    # def test_lab9_bisa_logout(self):
    #     response = Client().post('/logout/')
    #     self.assertEqual(response.status_code,302)