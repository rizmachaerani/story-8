from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home, name='home'),
    path('signup/', views.signup, name='signup'),
    path('login/', views.loginUser, name='loginUser'),
    path('loginFinish/', views.loginFinish , name ='loginFinish'),
    path('logout/', views.logout_user, name='logout'),
]