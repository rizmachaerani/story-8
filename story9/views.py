from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

# Create your views here.

def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'home.html')
    else:
        form = UserCreationForm()

    form = UserCreationForm()

    return render(request, 'signup.html', { 'form':form })

def loginUser(request):
    response = {}
    if request.method == "POST":
        user_id = request.POST['user_id']
        password = request.POST['password']
        user = authenticate(request, username=user_id, password=password)
        if user:
            login(request, user)
            return redirect('story9:loginFinish')
        else:
            response["user_not_found"]= "User doesn't exist"
            return render(request, 'login.html', response) 
    else:         
        return render(request,'login.html',response)

def loginFinish(request):
    response = {}
    response['user'] = request.user
    return render(request, 'logout.html', response)

def logout_user(request):
    if request.method == "POST":
        logout(request)
    return redirect('story9:loginUser')
